import test from 'ava';
import { Breadcrumb }from 'records/Records';
import reducer from 'reducers/SessionReducer';
import * as Actions from 'actions/SessionActions';

const username     = 'eljamesy';
const token        = 'myToken123';
const errorMsg     = 'Oh no!';
const title        = 'Five Go to Lunch';
const title2       = 'Five Have a Picnic';
const crumb        = Breadcrumb({ title });
const crumb2       = Breadcrumb({ title2 });

const openAction = {
  type: 'OPEN_SESSION',
  res:  {
    data: token
  },
  name: username
};
const openActionFail = {
  type:  'OPEN_SESSION_FAILURE',
  error: {
    data: {
      message: errorMsg
    }
  },
  name: username
};

const defaultState = reducer();
const openState    = reducer(defaultState, openAction);

test('open session', t => {
  const state = reducer(defaultState, openAction);
  t.is(state.get('name'), username);
  t.is(state.get('token'), token);
  t.is(state.get('legit'), true);
});

test('open session failure', t => {
  const state = reducer(defaultState, openActionFail);
  t.is(state.get('name'),  null);
  t.is(state.get('token'), null);
  t.is(state.get('legit'), false);
  t.is(state.get('error'), errorMsg);
});

test('close session', t => {
  const state = reducer(openState, Actions.close());
  t.is(state.get('name'),  null);
  t.is(state.get('token'), null);
  t.is(state.get('legit'), false);
});

test('validate session', t => {
  const state = reducer(openState, {
    type: 'VALIDATE_SESSION',
    res:  {
      data: username
    }
  });
  t.is(state.get('name'), username);
  t.is(state.get('token'), token);
  t.is(state.get('legit'), true);
});

test('validate session failure', t => {
  const state = reducer(openState, {
    type:  'VALIDATE_SESSION_FAILURE',
    error: {
      data: {
        message: errorMsg
      }
    }
  });
  t.is(state.get('name'),  null);
  t.is(state.get('token'), null);
  t.is(state.get('legit'), false);
  t.is(state.get('error'), errorMsg);
});

test('set loading', t => {
  const state = reducer(defaultState, Actions.setLoading(true));
  t.is(state.get('loading'), true);

  const state2 = reducer(state, Actions.setLoading(false));
  t.is(state2.get('loading'), false);
});

test('set night mode', t => {
  const state = reducer(defaultState, Actions.setNightMode(true));
  t.is(state.get('nightMode'), true);

  const state2 = reducer(state, Actions.setNightMode(false));
  t.is(state2.get('nightMode'), false);
});

test('push/pop bread crumb', t => {
  const state = reducer(defaultState, Actions.pushBreadcrumb(crumb));
  t.is(state.getIn(['breadcrumbs', 0]), crumb);

  const state2 = reducer(state, Actions.pushBreadcrumb(crumb2));
  t.is(state2.getIn(['breadcrumbs', 1]), crumb2);

  const state3 = reducer(state2, Actions.popBreadcrumb());
  t.is(state3.getIn(['breadcrumbs', 0]), crumb);
  t.is(state3.get('breadcrumbs').count(), 1);

  const state4 = reducer(state3, Actions.popBreadcrumb());
  t.is(state4.get('breadcrumbs').count(), 0);
});

test('flush breadcrumbs', t => {
  const state  = reducer(defaultState, Actions.pushBreadcrumb(crumb));
  const state2 = reducer(state, Actions.pushBreadcrumb(crumb2));
  const state3 = reducer(state2, Actions.flushBreadcrumbs());

  t.is(state3.get('breadcrumbs').count(), 0);
});
