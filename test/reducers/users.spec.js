import test from 'ava';
import reducer from 'reducers/UsersReducer';
import { Chapter } from 'records/Records';

const defaultState = reducer();

const chapters = [Chapter({ title: 'title' }), Chapter({ title: 'title2'})];
const users = [
  {
    username: 'Sally'
  },
  {
    username: 'Clive'
  }
];

test('get users', t => {
  const state = reducer(defaultState, {
    type: 'GET_USERS',
    res:  {
      data: users
    }
  });

  t.is(state.getIn(['users', 0]), users[0]);
  t.is(state.getIn(['users', 1]), users[1]);
});

test('get user stories', t => {
  const state = reducer(defaultState, {
    type: 'GET_USER_STORIES',
    res:  {
      data: chapters
    }
  });

  t.is(state.getIn(['stories', 0]), chapters[0]);
  t.is(state.getIn(['stories', 1]), chapters[1]);
});

test('register', t => {
  const successMsg = 'Yay!';

  const state = reducer(defaultState, {
    type: 'REGISTER',
    res:  {
      text: successMsg
    }
  });

  t.is(state.get('regSuccess'), successMsg);
});

test('register failure', t => {
  const errorMsg = 'Oops!';

  const state = reducer(defaultState, {
    type: 'REGISTER_FAILURE',
    error: {
      data: {
        message: errorMsg
      }
    }
  });

  t.is(state.get('regError'), errorMsg);
});
