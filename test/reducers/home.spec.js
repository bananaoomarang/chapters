import test from 'ava';
import { fromJS } from 'immutable';
import { Chapter }from 'records/Records';
import reducer from 'reducers/HomeReducer';

const chapters     = fromJS([Chapter({ title: 'title' }), Chapter({ title: 'title2'}), Chapter()]);
const defaultState = reducer();

test('get home stories', t => {
  const state = reducer(defaultState, {
    type: 'GET_HOME_STORIES',
    res:  {
      data: chapters
    }
  });

  t.is(state.get('stories'), chapters);
});
