import test from 'ava';
import { fromJS } from 'immutable';
import reducer from 'reducers/ChapterReducer';
import * as Actions from 'actions/ChapterActions';

const defaultState = reducer();

const chapter  = {
  title:  "Gravity's Rainbow",
  author: 'Thomas Pynchon'
};

const chapter2 = {
  title: 'Moby Dick',
  author: 'Herman Melville'
};

test('set Chapter', t => {
  // Chapter was set
  const state = reducer(defaultState, Actions.setChapter(chapter));
  t.is(state.getIn(['chapter', 'title']), chapter.title);
  t.is(state.getIn(['chapter', 'author']), chapter.author);

  // Chapter was changed slightly
  const state2 = reducer(state, Actions.setChapter({ author: chapter2.author }));
  t.is(state2.getIn(['chapter', 'title']), chapter.title);
  t.is(state2.getIn(['chapter', 'author']), chapter2.author);
});

test('set subchapters', t => {
  const newChapter = defaultState.get('chapter')
          .set('ordered', fromJS([chapter]))
          .set('unordered', fromJS([chapter2]));

  // Subchapters added
  // TODO should probably be separate test
  const state = reducer(defaultState, Actions.setChapter(newChapter));
  t.is(state.getIn(['chapter', 'ordered', 0, 'title']), chapter.title);
  t.is(state.getIn(['chapter', 'unordered', 0, 'title']), chapter2.title);

  // Edit ordered
  const state2 = reducer(state, Actions.setSubChapter('ordered', 0, { author: chapter2.author }));
  t.is(state2.getIn(['chapter', 'ordered', 0, 'title']), chapter.title);
  t.is(state2.getIn(['chapter', 'ordered', 0, 'author']), chapter2.author);

  // Edit unordered
  const state3 = reducer(state, Actions.setSubChapter('unordered', 0, { author: chapter.author }));
  t.is(state3.getIn(['chapter', 'unordered', 0, 'title']), chapter2.title);
  t.is(state3.getIn(['chapter', 'unordered', 0, 'author']), chapter.author);
});

test('set editing', t => {
  const state = reducer(defaultState, Actions.setEditing(true));
  t.is(state.get('editing'), true);

  const state2 = reducer(defaultState, Actions.setEditing(false));
  t.is(state2.get('editing'), false);
});

test('flush chapter', t => {
  const state  = reducer(defaultState, Actions.setChapter(chapter));
  const state2 = reducer(state, Actions.flushChapter());
  t.is(state2.getIn(['chapter', 'title']), '');
});
