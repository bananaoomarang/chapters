import React, { PropTypes } from 'react';
import { Link }             from 'react-router';
import capitalize           from 'lib/capitalize';
import { List }             from 'immutable';
import classSet             from 'classnames';
import { Motion, spring }   from 'react-motion';
import range                from 'lodash.range';
import EditableElement      from 'components/misc/EditableElement';

function reinsert(arr, from, to) {
  const _arr = arr.slice(0);
  const val  = _arr[from];

  _arr.splice(from, 1);
  _arr.splice(to, 0, val);

  return _arr;
}

function clamp(n, min, max) {
  return Math.max(Math.min(n, max), min);
}

const springConfig = {
  stiffness: 300,
  damping:   50
};

export default class ListView extends React.Component {
  static propTypes = {
    elements:   PropTypes.instanceOf(List).isRequired,

    reinsert:   PropTypes.func,
    onChange:   PropTypes.func,
    addNew:     PropTypes.func,

    header:     PropTypes.string,
    createUrl:  PropTypes.string,
    editing:    PropTypes.bool
  };

  state = {
    delta:       0,
    mouse:       0,
    isPressed:   false,
    lastPressed: 0,
    order:       [],
    itemHeight:  0
  };

  constructor(props) {
    super(props);

    this.bindSomeCheekyEvents = this.bindSomeCheekyEvents.bind(this);
    this.unbindEvents         = this.unbindEvents.bind(this);
    this.handleKeyup          = this.handleKeyup.bind(this);
    this.handlePointerDown    = this.handlePointerDown.bind(this);
    this.handleTouchStart     = this.handleTouchStart.bind(this);
    this.handleTouchMove      = this.handleTouchMove.bind(this);
    this.handlePointerMove    = this.handlePointerMove.bind(this);
    this.handlePointerUp      = this.handlePointerUp.bind(this);
    this.renderHeader         = this.renderHeader.bind(this);
    this.renderEditable       = this.renderEditable.bind(this);
  }

  componentDidMount() {
    if(this.props.editing)
      this.bindSomeCheekyEvents();

        // Pretty uggers, but otherwise clientHeight is innacurate
        setTimeout(() => {
          this.setState({
            order: range(this.props.elements.size + 1),
            itemHeight: document.querySelector('.list-item') ? document.querySelector('.list-item').clientHeight : 0
          });
        }, 1000);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.editing !== this.props.editing) {
      if (this.props.editing) {
        this.bindSomeCheekyEvents();
        this.setState({
          itemHeight: document.querySelector('.list-item') ? document.querySelector('.list-item').clientHeight : 0
        });
      }
      else {
        this.unbindEvents();
      }
    }

    if (this.props.elements.size !== prevProps.elements.size) {
      this.setState({
        order: range(this.props.elements.size + 1)
      });
    }
  }

  componentWillUnmount() {
    this.unbindEvents();
  }

  bindSomeCheekyEvents() {
    const container = this.refs.container;

    container.addEventListener('touchmove', this.handleTouchMove);
    window.addEventListener('touchend',  this.handlePointerUp);

    container.addEventListener('mousemove', this.handlePointerMove);
    window.addEventListener('mouseup',   this.handlePointerUp);

    window.onresize = () => {
      const itemHeight = document.querySelector('.list-item') ? document.querySelector('.list-item').clientHeight : 0;

      if(this.state.itemHeight !== itemHeight)
        this.setState({
          itemHeight
        });
    };
  }

  unbindEvents() {
    window.removeEventListener('touchend', this.handlePointerMove);
    window.removeEventListener('mouseup',   this.handlePointerUp);
  }

  handleKeyup(data, val) {
    const change = {
      index: data.index,
      changes: {
        [data.key]: val
      }
    };

    this.props.onChange(change);
  }

  handlePointerDown(pos, pressY, { pageY }) {
    this.setState({
      delta:       pageY - pressY,
      mouse:       pressY,
      isPressed:   true,
      lastPressed: pos
    });
  }

  handleTouchStart(key, pressLocation, e) {
    this.handlePointerDown(key, pressLocation, e.touches[0]);
  }

  handlePointerMove({ pageY }) {
    const { isPressed, delta, order, lastPressed, itemHeight } = this.state;

    if (isPressed) {
      const mouse    = pageY - delta;
      const row      = clamp(Math.round(mouse / itemHeight), 0, this.props.elements.size - 1);
      const newOrder = reinsert(order, order.indexOf(lastPressed), row);

      // This is to sync the real list
      if(order.indexOf(lastPressed) !== row)
        this.props.reinsert(order.indexOf(lastPressed), row);

      this.setState({
        mouse: mouse,
        order: newOrder
      });
    }
  }

  handleTouchMove(e) {
    e.preventDefault();

    this.handlePointerMove(e.touches[0]);
  }

  handlePointerUp() {
    this.setState({
      isPressed:   false,
      delta:       0
    });
  }

  renderHeader() {
    if(this.props.header) {
      return <h2>{this.props.header}</h2>;
    }

    return null;
  };

  renderEditable(classes) {
    const { mouse, isPressed, lastPressed, order, itemHeight } = this.state;

    const headerStyle = {
      display:     'inline-block',
      fontSize:    '1em',
      marginBottom: 0,
    };

    const elements = this.props.elements
                         .concat([{
                           title:       'New',
                           uneditable:  true
                         }]);



    return (
      <div className={classes.container} ref="container" style={{ height: `${elements.size*itemHeight + 40}px` }}>
        <div className="header">
          {this.renderHeader()}
        </div>

        <ul>
          { range(elements.size).map(i => {
              const isNewButton = (i === elements.size - 1);

              const style = (lastPressed === i && isPressed) ?
                            {
                              scale:  spring(1.03, springConfig),
                              shadow: spring(6, springConfig),
                              y:      mouse
                            }
                            :
                            {
                              scale:  spring(1, springConfig),
                              shadow: spring(0, springConfig),
                              y:      spring((isNewButton ? i : order.indexOf(i)) * itemHeight, springConfig)
                            };

              return (
                <Motion style={style} key={i}>
                  {({ scale, shadow, y }) => {
                     const element   = elements.get(isNewButton ? i : order.indexOf(i));
                     let subElements = [];

                     const prefix = isNewButton ? '' : (order.indexOf(i) + 1) + '. ';

                     subElements.push(
                       <div className="title" key="title">
                         <span>
                           {prefix}
                         </span>
                         <EditableElement
                           tag="span"
                           style={headerStyle}
                           content={element.title}
                           placeholder="Untitled"
                           editing={!element.uneditable}
                           update={this.handleKeyup.bind(this, { key: 'title', index: i })}
                         />
                       </div>
                     );

                     subElements.push(
                       <span className="separator" key="separator">
                         &nbsp;
                       </span>
                     );

                     subElements.push(
                       <EditableElement
                         tag="span"
                         className="description"
                         key="description"
                         content={element.description}
                         placeholder="In which&hellip;"
                         editing={!element.uneditable}
                         update={this.handleKeyup.bind(this, { key: 'description', index: i })}
                       />
                     );

                     if(element.author || !element.uneditable) {
                       subElements.push(
                         <div className="author" key="author">
                           <span>
                             By&nbsp;
                           </span>
                           <EditableElement
                             tag="span"
                             content={element.author}
                             placeholder="Tom Sawyer"
                             editing={!element.uneditable}
                             update={this.handleKeyup.bind(this, { key: 'author', index: i })}
                           />
                         </div>
                       );
                     }

                     const liStyle = {
                       boxShadow:       `rgba(0, 0, 0, 0.8) ${shadow}px ${shadow}px 0px 0px`,
                       transform:       `translate3d(0, ${y}px, 0) scale(${scale})`,
                       WebkitTransform: `translate3d(0, ${y}px, 0) scale(${scale})`,
                       zIndex:          (i === lastPressed) ? 99 : i
                     };

                     if(isNewButton) {
                       return (
                         <li
                           className="clickable list-item"
                           style={liStyle}
                           onClick={this.props.addNew}>
                           {subElements}
                         </li>
                       );
                     }
                     return (
                       <li
                         className={classes.listItem}
                         onMouseDown={  isNewButton ? ()=>({}) : this.handlePointerDown.bind(null, i, y)}
                         onTouchStart={ isNewButton ? ()=>({}) : this.handleTouchStart.bind(null, i, y)}
                         style={{ ...liStyle, backgroundColor: '#fff' }}>
                         {subElements}
                       </li>
                     );
                   }
                  }
                </Motion>
              );
            })
          }
        </ul>
      </div>
    );
  }

  render() {
    const classes = {
      container: classSet({
        list:               true,
        ['no-user-select']: this.props.editing,
        ['list-editing']:   this.props.editing
      }),
      listItem: classSet({
        ['list-item']: true,
        ['editing']:   this.props.editing,
        ['grabbing']:  this.state.isPressed
      })
    };

    if(this.props.editing) {
      return this.renderEditable(classes);
    }

    return (
      <div className={classSet(classes.container)} ref="container">
        <div className="header">
          {
            this.props.header ? <h2>{this.props.header}</h2> : null
          }
        </div>

        <ul>
          {
            this.props.elements.map((element, index) => {
              let subElements = [];

              if(element.title)
                subElements.push(
                  <span className="title" key="title">
                    {index + 1 + '. ' + capitalize(element.title)}
                  </span>
                );

              if(element.description) {
                subElements.push(
                    <span className="separator" key="separator">
                      &nbsp;
                    </span>
                );

                subElements.push(
                  <span className="description" key="description">
                    <em>{element.description}</em>
                  </span>
                );
              }

              if(element.author)
                subElements.push(
                  <div className="author" key="author">
                    <span>
                      By&nbsp;
                    </span>
                    <span>
                      {element.author}
                    </span>
                  </div>
                );

              return (
                <Link to={element.href} key={index}>
                  <li ref={'listitem-' + index} data-index={index} className="clickable list-item">
                    {subElements}
                  </li>
                </Link>
              );
            })
          }
        </ul>
      </div>
    );
  }
}
