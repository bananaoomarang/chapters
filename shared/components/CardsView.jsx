import React, { PropTypes } from 'react';
import { Link }             from 'react-router';
import classSet             from 'classnames';
import { List }             from 'immutable';
import EditableElement      from 'components/misc/EditableElement';

export default class CardsView extends React.Component {
  static propTypes = {
    elements:   PropTypes.instanceOf(List).isRequired,
    id:         PropTypes.string,
    header:     PropTypes.string,
    subheader:  PropTypes.string,
    emptyMsg:   PropTypes.string,
    createUrl:  PropTypes.string,
    onReorder:  PropTypes.func,
    onChange:   PropTypes.func,
    editing:    PropTypes.bool,
    handleSave: PropTypes.func,
    addNew:     PropTypes.func
  };

  state = {
    dragging: false
  };

  constructor(props) {
    super(props);

    this.bindSomeCheekyEvents = this.bindSomeCheekyEvents.bind(this);
    this.handlePointerDown    = this.handlePointerDown.bind(this);
    this.handlePointerMove    = this.handlePointerMove.bind(this);
    this.handlePointerUp      = this.handlePointerUp.bind(this);
    this.handleKeyup          = this.handleKeyup.bind(this);
    this.renderCard           = this.renderCard.bind(this);
  }

  componentDidMount() {
    if(!this.props.editing || this.state.bound)
      return;

    this.bindSomeCheekyEvents();
  };

  componentDidUpdate(prevProps) {
    if(!prevProps.editing && this.props.editing) {
      this.bindSomeCheekyEvents();
    }

    if(prevProps.editing && !this.props.editing) {
      this.unbindEvents();
    }
  };

  componentWillUnmount() {
      this.unbindEvents();
  };

  bindSomeCheekyEvents() {
    const container = this.refs.container;

    container.addEventListener('touchmove', this.handlePointerMove);
    window.addEventListener('touchend',     this.handlePointerUp);

    container.addEventListener('mousemove', this.handlePointerMove);
    window.addEventListener('mouseup',      this.handlePointerUp);

    for (let r in this.refs) {
      if(/^card-/.test(r)) {
        this
          .refs[r]
          .onmousedown = this.handlePointerDown;
      }
    }
  };

  unbindEvents() {
    window.removeEventListener('touchmove', this.handlePointerMove);
    window.removeEventListener('mouseup',   this.handlePointerUp);
  };

  handlePointerDown(e) {
    this.setState({ dragging: e.currentTarget });
  };

  handlePointerMove(e) {
    if(!this.state.dragging)
      return;

    const EL_HEIGHT = this.state.dragging.clientHeight;
    // const EL_WIDTH  = this.state.dragging.clientWidth;
    const cards     = this.refs.cards;

    const yPosition   = e.clientY - e.offsetY;
    // const xPosition   = e.clientX - e.offsetX;

    const index = this.state.dragging.dataset.index;
    const insertAt = Math.round(yPosition / EL_HEIGHT) - cards.offsetY;

    if(index !== insertAt) {
      this.props.onReorder(index, insertAt);
      this.setState({ dragging: this.refs['card-' + insertAt] });
    }
  };

  handlePointerUp() {
    this.setState({ dragging: false });
  };

  handleKeyup(data, val) {
    const change = {
      index: data.index,
      changes: {
        [data.key]: val
      }
    };

    this.props.onChange(change);
  };

  renderCard(card, index) {
    const { editing } = this.props;

    const classes = {
      wrapper: classSet({
        ['card-wrapper']: true,
        ['card-editing']: this.props.editing
      }),
      card: classSet({
        ['card']:      true,
        ['clickable']: (!editing || card.uneditable)
      })
    };

    const markEditable = (editing && !card.uneditable) ? ' card-editable' : '';

    const showAuthor = (card.author || editing) && !card.uneditable;

    const innerCard = (
      <div className={classes.card} onClick={card.onClick || (n=>n)}>
        <div className="card-header">
          <EditableElement
            className={'card-header-row' + markEditable}
            tag="h2"
            content={card.title}
            placeholder="Untitled"
            editing={editing && !card.uneditable}
            update={this.handleKeyup.bind(this, { key: 'title', index: index })}
          />
          {
            showAuthor ?
              (
                <div className="card-header-row">
                  <span>By </span>
                  <EditableElement
                    className={'inline-block' + markEditable}
                    tag="h3"
                    content={card.author}
                    placeholder="Herman Melville"
                    editing={editing && !card.uneditable}
                    update={this.handleKeyup.bind(this, { key: 'author', index: index })}
                  />
                </div>
              ) : null
          }
        </div>
        <div className="card-body-wrapper">
          <EditableElement
            className={'card-body' + markEditable}
            tag="span"
            content={card.body}
            placeholder="In which&hellip;"
            editing={editing && !card.uneditable}
            update={this.handleKeyup.bind(this, { key: 'description', index: index })}
          />
        </div>
      </div>
    );

    return (
      <div className={classes.wrapper} key={index} ref={['card', index].join('-')} data-index={index}>
        {
          ((editing && !card.uneditable) || card.onClick) ?
            innerCard
            :
            <Link to={card.href}>{innerCard}</Link>
        }
      </div>
    );
  }

  render() {
    const classes = {
      container: {
        ['no-user-select']: this.props.editing
      }
    };

    const addCard = this.props.editing ? [{
      title:      'New',
      body:       '+',
      href:       this.props.createUrl,
      onClick:    this.props.addNew,
      uneditable: true
    }] : [];

    const cards = this.props.elements
                      .concat(addCard)
                      .map(this.renderCard);

    return (
      <div id={this.props.id || 'CardsView'} className={classSet(classes.container)} ref="container">
        {
          this.props.header ?
            (
              <div id="header">
                <h2>{this.props.header}</h2>
              </div>
            ) : null
        }

        {
          this.props.subheader ?
            <h3 id="subheader">{this.props.subheader}</h3> : null
        }

        {
          cards.size  ?
            <div className="cards" ref="cards">{cards}</div>
            :
            <h2>{this.props.emptyMsg}</h2>
        }
      </div>
    );
  }
}
