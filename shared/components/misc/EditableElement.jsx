import React, { PropTypes }  from 'react';
import classSet              from 'classnames';
import getCaret              from 'lib/getCaret';
import ifdefBrowser          from 'lib/ifdefBrowser';
import debounce               from 'debounce';
const {
  handleSingleQuote,
  handleDoubleQuote,
  handleElipsis
} = ifdefBrowser(() => require('lib/cheeky-keys'));

export default class EditableElement extends React.Component {
  static propTypes = {
    update:      PropTypes.func.isRequired,
    editing:     PropTypes.bool.isRequired,
    placeholder: PropTypes.string.isRequired,
    tag:         PropTypes.string.isRequired,

    className:   PropTypes.string,
    content:     PropTypes.string,
    style:       PropTypes.object,
  };

  state = {
    clicked: false
  };

  constructor(props) {
    super(props);

    this.handleClick   = this.handleClick.bind(this);
    this.handleBlur    = this.handleBlur.bind(this);
    this.handleFocus   = this.handleFocus.bind(this);
    this.handleKeydown = this.handleKeydown.bind(this);
    this.handleKeyup   = debounce(this.handleKeyup.bind(this), 300);
  }

  handleClick(e) {
    e.preventDefault();
    e.stopPropagation();


    this.handleFocus();
  }

  handleBlur() {
    const elDOM = this.refs.element;

    this.setState({
      clicked: false
    });

    elDOM.removeEventListener('keydown', this.handleKeydown);
    elDOM.removeEventListener('keyup', this.handleKeyup);

    this.props.update(elDOM.textContent);
  };

  handleFocus() {
    const elDOM = this.refs.element;

    this.setState({ clicked: true }, function () {
      elDOM.focus();
      if(!elDOM.textContent) {
        getCaret(elDOM).setPosition('start');
      }
    });

    elDOM.addEventListener('keydown', this.handleKeydown);
    elDOM.addEventListener('keyup', this.handleKeyup);
  };

  handleKeydown(e) {
    const elDOM = this.refs.element;

    switch(e.key) {
      case 'Enter':
        e.preventDefault();
        e.stopPropagation();
        break;
      case "'":
        setTimeout(() => {
          handleSingleQuote(elDOM, getCaret(elDOM).position);
        })
        break;
      case '"':
        setTimeout(() => {
          handleDoubleQuote(elDOM, getCaret(elDOM).position);
        })
        break;
      case '.':
        setTimeout(() => {
          handleElipsis(elDOM, getCaret(elDOM).position);
        })
        break;
    }
  }

  handleKeyup() {
    if(this.state.clicked) {
      this.props.update(this.refs['element'].textContent);
    }
  }

  render () {
    const classes = classSet({
      greyed: !this.props.content
    });

    const editClasses = classSet({
      greyed: !this.state.clicked && !this.props.content
    });

    if(this.props.editing) {
      const content = this.state.clicked ?
                        (this.props.content || '') :
                        (this.props.content || this.props.placeholder);
      return React.createElement(
        this.props.tag,
        {
          ref:                            'element',
          style:                          this.props.style,
          className:                      [editClasses, this.props.className].join(' '),
          contentEditable:                true,
          suppressContentEditableWarning: true,
          onClick:                        this.handleClick,
          onFocus:                        this.handleFocus,
          onBlur:                         this.handleBlur
        },
        content
      );
    }

      return React.createElement(
        this.props.tag,
        {
          ref:       'element',
          style:     this.props.style,
          className: [classes, this.props.className].join(' '),
          onFocus:   this.handleFocus
        },
        (this.props.content)
      );
  }
}
