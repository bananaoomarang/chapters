import { fromJS }  from 'immutable';
import { Chapter } from 'records/Records';

const defaultState = fromJS({
  chapter: new Chapter(),

  // Global styles
  alignment: 'left',
  font:      {
    size: 24
  },

  // Stories it is possible to edit with current permissions
  userStories: [],

  // Interface state
  editing:   false
});

export default function chapterReducer(state = defaultState, action = {}) {
  console.log(action.type);

  switch(action.type) {
    case 'GET_CHAPTER':
      return chapterReducer(state, {
        type: 'SET_CHAPTER',
        chapter: action.res.data
      });

    case 'POST_CHAPTER':
      return chapterReducer(state, {
        type: 'SET_CHAPTER',
        chapter: action.res.data
      });

    case 'PATCH_CHAPTER':
      return chapterReducer(state, {
        type: 'SET_CHAPTER',
        chapter: action.diff
      });

    case 'SET_CHAPTER':
      return state
        .mergeDeepIn(['chapter'], fromJS(action.chapter));

    case 'SET_SUBCHAPTER':
      return state
        .mergeDeepIn(['chapter', action.subType, action.index], fromJS(action.chapter));

    case 'SET_EDITING':
      return state.set('editing', action.editing);

    case 'FLUSH_CHAPTER':
      return state.set('chapter', new Chapter());

    default:
      return state;
  }
}
