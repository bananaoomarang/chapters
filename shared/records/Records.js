import { Record, List } from 'immutable';

export const Chapter = Record({
    id:               null,     // Database ID
    public:           false,
    read:             false,
    write:            false,
    title:            '',
    description:      '',
    markdown:         '',     // Markdown
    html:             '',     // HTML from ^
    author:           '',
    depends:          List(), // Other chapters this one depends on having read
    ordered:          List(),
    unordered:        List(),
    isOrdered:        false,
    wordCount:        0       // Not currently implemented
}, 'Chapter');

export const Breadcrumb = Record({
  id:    '',
  title: ''
}, 'Breadcrumb');
