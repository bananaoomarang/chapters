// @flow
export default function fetchComponentData(dispatch: Function, components: Array<Object>, params: Object) {
  const needs = components.reduce( (prev, current) => {
    const currentNeeds        = current.needs ?
            current.needs(params) : [];
    const currentWrappedNeeds = (current.WrappedComponent && current.WrappedComponent.needs) ?
            current.WrappedComponent.needs(params) : [];

    return prev.concat(currentNeeds).concat(currentWrappedNeeds);
  }, []);

  const promises = needs.map(need => dispatch(need));

  return Promise.all(promises);
}
